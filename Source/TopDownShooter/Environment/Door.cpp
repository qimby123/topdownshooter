// Fill out your copyright notice in the Description page of Project Settings.


#include "Door.h"
#include "Components/BoxComponent.h"
#include "Components/SceneComponent.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
ADoor::ADoor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	/*CollisionComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("Collision Component"));
	RootComponent = CollisionComponent;*/

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));

	DoorMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Door Mesh"));
	DoorMesh->SetupAttachment(RootComponent);
	IsOpening = false;
}

// Called when the game starts or when spawned
void ADoor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ADoor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (IsOpening) {
		FVector CrntLoc = DoorMesh->RelativeLocation;
		if (FVector::Dist(FVector(0.f), CrntLoc) < OpeningLimit)
			DoorMesh->SetRelativeLocation(CrntLoc + OpeningRelativeDirection * OpeningVelosity * DeltaTime);
		else
			IsOpening = false;
	}
}

void ADoor::Interact(ATopDownShooterCharacter* InteractingActor)
{
	IsOpening = true;
}

