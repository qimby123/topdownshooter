// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "InteractiveObject.h"
#include "Door.generated.h"

class UBoxComponent;

UCLASS()
class TOPDOWNSHOOTER_API ADoor : public AActor, public IInteractiveObject
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADoor();



protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	/*UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UBoxComponent* CollisionComponent;*/

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	UStaticMeshComponent* DoorMesh;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FVector OpeningRelativeDirection = FVector(1.f, 0.f, 0.f);

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float OpeningVelosity = 10.f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float OpeningLimit = 100.f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool IsOpening = false;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	virtual void Interact(ATopDownShooterCharacter* InteractingActor);


};
