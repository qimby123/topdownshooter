// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "InteractiveObject.generated.h"

class ATopDownShooterCharacter;

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UInteractiveObject : public UInterface
{
	GENERATED_BODY()
};

/**
 * Objects that the player will interact using the Interaction button
 */
class TOPDOWNSHOOTER_API IInteractiveObject
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	
	// The main logic method for interacting with this object
	virtual void Interact(ATopDownShooterCharacter* InteractingActor) PURE_VIRTUAL(IInteractiveObject::Interact, return;);
};
