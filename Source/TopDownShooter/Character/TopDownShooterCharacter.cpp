// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "TopDownShooterCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "DrawDebugHelpers.h"
#include "Environment/InteractiveObject.h"

ATopDownShooterCharacter::ATopDownShooterCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->bAbsoluteRotation = true; // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->RelativeRotation = FRotator(-60.f, 0.f, 0.f);
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Blueprint/Character/M_Cursor_Decal.M_Cursor_Decal'"));
	
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ATopDownShooterCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	if (CursorToWorld != nullptr)
	{
		if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
		{
			if (UWorld* World = GetWorld())
			{
				FHitResult HitResult;
				FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
				FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
				FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
				Params.AddIgnoredActor(this);
				World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
				FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
				CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
			}
		}
		else if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);
		}
	}

	MovementTick(DeltaSeconds);
	ZoomTick(DeltaSeconds);
}

void ATopDownShooterCharacter::SetupPlayerInputComponent(UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

	InputComponent->BindAxis(TEXT("MoveForward"), this, &ATopDownShooterCharacter::InputAxisX);
	InputComponent->BindAxis(TEXT("MoveRight"), this, &ATopDownShooterCharacter::InputAxisY);
	InputComponent->BindAxis(TEXT("Zoom"), this, &ATopDownShooterCharacter::Zoom);

	InputComponent->BindAction(TEXT("Interact"), EInputEvent::IE_Pressed, this, &ATopDownShooterCharacter::Interacted);
}

void ATopDownShooterCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}

void ATopDownShooterCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}

void ATopDownShooterCharacter::Zoom(float Value)
{
	CameraHeight -= Value * CameraZoomScale;
}

void ATopDownShooterCharacter::MovementTick(float DeltaTime)
{
	if (MovementState == EMovementState::Run_State) {
		float coef = (GetVelocity().GetSafeNormal() * GetActorForwardVector()).Size();
		float ResSpeed = MovementInfo.WalkSpeed + coef*(MovementInfo.RunSpeed - MovementInfo.WalkSpeed);
		GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
	}

	FVector cam_forward_vector = CameraBoom->GetForwardVector();
	cam_forward_vector.Z = 0;
	cam_forward_vector.Normalize();

	FVector cam_right_vector = CameraBoom->GetRightVector();
	cam_right_vector.Z = 0;
	cam_right_vector.Normalize();
	
	AddMovementInput(cam_forward_vector, AxisX);
	AddMovementInput(cam_right_vector, AxisY);

	APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	
	if (!IsValid(myController))
		return;

	FVector RayOrigin, RayDirection;
	if (!myController->DeprojectMousePositionToWorld(RayOrigin, RayDirection))
		return;
	
	FVector res = GetRayAndPlaneIntersactionPoint(RayOrigin, RayDirection, 500000, GetActorLocation(), FVector(.0f, .0f, 1.0f));
	FHitResult ResultHit;
	myController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, ResultHit);
	float FindLookAtRotationResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), res).Yaw;
	SetActorRotation(FQuat(FRotator(0.0f, FindLookAtRotationResultYaw, 0.0f)));
}

void ATopDownShooterCharacter::ZoomTick(float DeltaTime)
{
	float crnt_height = CameraBoom->TargetArmLength;
	CameraBoom->TargetArmLength = FMath::FInterpTo(crnt_height, CameraHeight, DeltaTime, CameraZoomSmoothCoef);
}

void ATopDownShooterCharacter::CharacterUpdate()
{
	float ResSpeed = 600.0f;
	switch (MovementState)
	{
	case EMovementState::Walk_State:
		ResSpeed = MovementInfo.WalkSpeed;
		break;
	case EMovementState::Run_State:
		ResSpeed = MovementInfo.RunSpeed;
		break;
	case EMovementState::Aim_State:
		ResSpeed = MovementInfo.AimSpeed;
		break;
	default:
		break;
	}

	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void ATopDownShooterCharacter::ChangeMovementState(EMovementState NewState)
{
	MovementState = NewState;
	CharacterUpdate();
}

FVector ATopDownShooterCharacter::GetRayAndPlaneIntersactionPoint(FVector RayOrigin, FVector RayDirection, float Range, FVector PlaneOrigin, FVector PlaneNormal)
{
	PlaneOrigin -= RayOrigin;
	float dot1 = Dot3(PlaneOrigin, PlaneNormal);

	RayDirection *= Range;

	float dot2 = Dot3(RayDirection, PlaneNormal);

	RayDirection *= dot1 / dot2;
	RayDirection += RayOrigin;
	return RayDirection;
}

void ATopDownShooterCharacter::Interacted()
{
	FHitResult OutHit;
	FVector Start = GetActorLocation();
	FVector End = ((GetActorForwardVector() * 250.f) + Start);
	FCollisionQueryParams TraceParams;
	//DrawDebugLine(GetWorld(), Start, End, FColor::Green, false, 1, 0, 1);
	if (GetWorld()->LineTraceSingleByChannel(OutHit, Start, End, ECollisionChannel::ECC_Visibility, TraceParams))
	{
		IInteractiveObject* InteractiveObject = Cast<IInteractiveObject>(OutHit.GetActor());
		if (InteractiveObject) {
			InteractiveObject->Interact(this);
		}
	}
}
