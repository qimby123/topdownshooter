// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TopDownShooterPlayerController.generated.h"

UCLASS()
class ATopDownShooterPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ATopDownShooterPlayerController();

protected:


	// Begin PlayerController interface
	virtual void PlayerTick(float DeltaTime) override;
	virtual void SetupInputComponent() override;

};


